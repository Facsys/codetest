using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;

namespace CodeTest
{
    class Program
    {
        private static readonly string DEFAULT_DBNAME = "SurfaceRoughnessDB";
        private static readonly string DEFAULT_DBPATH = "C:\\temp";
        private static readonly float DEFAULT_FILTER = 3F;

        static void Main(string[] args)
        {
            string DBName;
            string DirPath = "";
            string InputFilePath = "";
            float Filter = DEFAULT_FILTER;
            bool check = true;
            try
            {
                while (check)
                {
                    Console.WriteLine($"What is the name of the database? ({DEFAULT_DBNAME})");
                    DBName = Console.ReadLine();
                    if (string.IsNullOrEmpty(DBName))
                    {
                        DBName = DEFAULT_DBNAME;
                    }
                    Console.WriteLine($"What is the file path of the folder the database is in? ({DEFAULT_DBPATH})");
                    DirPath = Console.ReadLine().Replace("\"", "");
                    if (string.IsNullOrEmpty(DirPath))
                    {
                        DirPath = DEFAULT_DBPATH;
                    }
                    Console.WriteLine($"What is the standard deviation filter size? ({DEFAULT_FILTER.ToString("F2")})");
                    var filterSrting = Console.ReadLine();
                    if (string.IsNullOrEmpty(filterSrting))
                    {
                        Filter = DEFAULT_FILTER;
                    }
                    else
                    {
                        if (!float.TryParse(filterSrting, out Filter))
                        {
                            Console.WriteLine("Invalid count. Please Try again");
                            break;
                        }
                    }
                    InputFilePath = Path.Combine(DirPath, DBName + ".DB3");
                    if (!checkIfValid(InputFilePath))
                    {
                        Console.WriteLine("Inaccessable file path. Please Try again");
                    }
                    else
                    {
                        check = false;
                    }
                }

                SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + InputFilePath);
                if (args.Any())
                {
                    CreateData(sqlite);
                }
                else
                {
                    var outputLines = new List<string>();
                    outputLines.Add(Test.HeaderRow);
                    var tests = GetTestlist(sqlite, Filter);

                    foreach (var test in tests)
                    {
                        outputLines.Add(test.SummarizeIntoCSV());
                    }

                    var OutputFilePath = Path.Combine(DirPath, "Output.csv");
                    File.WriteAllLines(OutputFilePath, outputLines.ToArray());
                    Console.WriteLine($"The summary report was generated and can be found at {OutputFilePath}.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }


        private static void CreateData(SQLiteConnection sqlite)
        {
            var scale = .044;
            var mean = .5;
            var stdDev = .05;
            var rand = new Random();
            sqlite.Open();
            var now = DateTime.Now;
            for (var i = 0; i < 10; i++)
            {
                SQLiteCommand command = new SQLiteCommand($"INSERT INTO Tests (stime, planeId, Operator) " +
                    $"VALUES ('{FormatDateTime(now.AddDays(-(rand.Next(10))).AddMinutes(-(rand.Next(600))))}', '{Guid.NewGuid().ToString().Replace("-", "")}', '')", sqlite);
                command.ExecuteNonQuery();
                var test_uid = sqlite.LastInsertRowId;
                for (var x = 0; x < 100; x++)
                {
                    for (var y = 0; y < 10; y++)
                    {
                        //gaussian random
                        double u1 = 1.0 - rand.NextDouble();
                        double u2 = 1.0 - rand.NextDouble();
                        double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
                        double randNormal = mean + stdDev * randStdNormal;

                        command = new SQLiteCommand($"INSERT INTO Measurements (test_uid, x, y, height) " +
                            $"VALUES ({test_uid}, {x * scale}, {y * scale}, {randNormal})", sqlite);
                        command.ExecuteNonQuery();
                    }
                }
            }
            sqlite.Close();
        }

        private static string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private static IList<Test> GetTestlist(SQLiteConnection sqlite, float Filter)
        {
            var ret = new List<Test>();
            sqlite.Open();
            SQLiteCommand command = new SQLiteCommand("SELECT t.test_uid, planeID, operator, sTime, height, x, y FROM Tests t " +
                "LEFT JOIN Measurements m ON m.test_uid = t.test_uid " +
                "ORDER BY sTime ASC", sqlite);
            SQLiteDataReader reader = command.ExecuteReader();
            var test_uidOrd = reader.GetOrdinal("test_uid");
            var planeIDOrd = reader.GetOrdinal("planeID");
            var operatorOrd = reader.GetOrdinal("operator");
            var sTimeOrd = reader.GetOrdinal("sTime");
            var heightOrd = reader.GetOrdinal("height");
            var xOrd = reader.GetOrdinal("x");
            var yOrd = reader.GetOrdinal("y");
            while (reader.Read())
            {
                var thisTest_uid = reader.GetInt32(test_uidOrd);
                var thisTest = ret.Where(x => x.Test_uid == thisTest_uid).FirstOrDefault();
                if (thisTest == null)
                {
                    thisTest = new Test()
                    {
                        Test_uid = thisTest_uid,
                        PlaneID = reader.GetString(planeIDOrd),
                        Operator = reader.IsDBNull(operatorOrd) ? "" : reader.GetString(operatorOrd),
                        Time = reader.GetDateTime(sTimeOrd),
                        Filter = Filter

                    };
                    ret.Add(thisTest);
                }
                if (!reader.IsDBNull(heightOrd))
                {
                    thisTest.Measurements.Add(new Measurement()
                    {
                        Height = reader.GetFloat(heightOrd),
                        X = reader.GetFloat(xOrd),
                        Y = reader.GetFloat(yOrd),
                    });
                }
            }
            Console.WriteLine("Number of valid tests in the Database: " + ret.Where(x => x.IsValid).Count());
            sqlite.Close();
            return ret;
        }

        //Makes sure the file path is valid
        public static bool checkIfValid(string FilePath)
        {
            //Credit where credit is due, I got this method on stack overflow when trying to figure out the best way to do this.
            //https://stackoverflow.com/questions/38519688/how-do-i-check-if-a-file-is-a-sqlite-database-in-c
            try
            {
                byte[] bytes = new byte[17];
                using (System.IO.FileStream fs = new System.IO.FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    fs.Read(bytes, 0, 16);
                }
                string chkStr = System.Text.ASCIIEncoding.ASCII.GetString(bytes);
                return chkStr.Contains("SQLite format");
            }
            catch
            {
                return false;
            }
        }
    }
}
