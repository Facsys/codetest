﻿namespace CodeTest
{
    public class Measurement
    {
        public float Height { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
    }
}
