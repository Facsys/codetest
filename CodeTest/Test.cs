﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeTest
{
    public class Test
    {
        internal static string HeaderRow = "DateTime,PlaneID,Operator,MinMeasurementX,MinMeasurementY,MinMeasurementHeight,MaxMeasurementX,MaxMeasurementY,MaxMeasurementHeight,Mean,HeightRange,AverageRoughness,RMSRoughness,CountInsideFilter,CountOutside";
        private float? _meanHeight;
        private IList<Measurement> _filteredMeasurements;

        public Test()
        {
            Measurements = new List<Measurement>();
        }
        public DateTime Time { get; set; }
        public string PlaneID { get; set; }
        public string Operator { get; set; }
        public IList<Measurement> Measurements { get; set; }
        public IList<Measurement> FilteredMeasurements
        {
            get
            {
                if (_filteredMeasurements == null && Filter.HasValue && IsValid)
                {
                    var stdDev = UnfilteredStdDev;
                    _filteredMeasurements = Measurements.Where(x =>
                        x.Height >= UnfilteredMeanHeight - (Filter.Value * stdDev) &&
                        x.Height <= UnfilteredMeanHeight + (Filter.Value * stdDev)
                    ).ToList();
                }
                return _filteredMeasurements;
            }
        }
        public float? Filter { get; internal set; }
        public int Test_uid { get; internal set; }
        public bool IsValid { get { return Measurements.Count == 1000; } }
        public Measurement MinMeasurement { get { return FilteredMeasurements.OrderBy(x => x.Height).First(); } }
        public Measurement MaxMeasurement { get { return FilteredMeasurements.OrderBy(x => x.Height).Last(); } }

        public float AverageRoughness
        {
            get
            {
                var avg = FilteredMeasurements.Average(x => x.Height);
                return FilteredMeasurements.Average(x => Math.Abs(x.Height - avg));
            }
        }

        public float UnfilteredStdDev
        {
            get
            {
                var avg = Measurements.Average(x => x.Height);
                return (float)Math.Sqrt(Measurements.Average(x => Math.Pow(x.Height - avg, 2.0)));
            }
        }

        public float RMSRoughness
        {
            get
            {
                var avg = FilteredMeasurements.Average(x => x.Height);
                return (float)Math.Sqrt(FilteredMeasurements.Average(x => Math.Pow(x.Height - avg, 2.0)));
            }
        }

        public float UnfilteredMeanHeight
        {
            get
            {
                if (_meanHeight == null)
                {
                    _meanHeight = Measurements.Average(x => x.Height);
                }
                return _meanHeight.Value;
            }
        }

        internal string SummarizeIntoCSV()
        {
            var ret = new StringBuilder();
            ret.Append(Time).Append(",");

            ret.Append(PlaneID).Append(",");

            ret.Append(Operator).Append(",");
            if (IsValid)
            {
                ret.Append(MinMeasurement.X).Append(",")
                    .Append(MinMeasurement.Y).Append(",")
                    .Append(MinMeasurement.Height).Append(",");

                ret.Append(MaxMeasurement.X).Append(",")
                    .Append(MaxMeasurement.Y).Append(",")
                    .Append(MaxMeasurement.Height).Append(",");

                ret.Append(FilteredMeasurements.Average(x => x.Height)).Append(",");

                ret.Append(MaxMeasurement.Height - MinMeasurement.Height).Append(",");

                ret.Append(AverageRoughness).Append(",");

                ret.Append(RMSRoughness).Append(",");

                ret.Append(FilteredMeasurements.Count).Append(",");
                ret.Append(Measurements.Count - FilteredMeasurements.Count);


            }
            else
            {
                ret.Append("Invalid Test");
            }
            return ret.ToString();
        }
    }
}
